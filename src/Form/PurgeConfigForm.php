<?php

namespace Drupal\audit_trail_purge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class PurgeConfigForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'audit_trail_purge.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'audit_trail_purge_config_form';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form['no_of_days'] = array(
      '#type' => 'textfield',
      '#default_value' => $config->get('no_of_days'),
      '#attributes' => array(
        ' type' => 'number',
      ),
      '#title' => 'Number of days to keep the logs',
      '#description' => t('Add the number for how many days you want to keep the logs. Logs betond these days will be purged'),
      '#required' => true,
      '#maxlength' => 2
    );
    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('no_of_days', $form_state->getValue('no_of_days'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}